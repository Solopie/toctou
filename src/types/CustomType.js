// I really liked the way JamesApple created a super-class with a custom exception in Javascript so I will be influenced greatly by that pattern here.
// https://gitlab.com/james262/mass-assignment/-/blob/master/src/user.js

const ValidationError = require("./ValidationError");

class CustomType {
    // This function needs to be overwritten or else type will never be valid
    isValid() {
        return false;
    }

    validate() {
        // Ensure values don't change whilst validating the type
        if(this.isValid()) {
            Object.freeze(this); 
            return this;
        } else {
            throw new ValidationError("Validation has failed for a custom type");
        }
    }
}


module.exports = { CustomType };
