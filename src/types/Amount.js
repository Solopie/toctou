const { CustomType } = require("./CustomType");

class Amount extends CustomType {
    constructor(value) {
        super();
        this.value = value;
        this.validate();
    }

    isValid() {
        this.value = parseInt(this.value);

        return (
            typeof this.value === "number" &&
            this.value > 0
        );
    }
}

module.exports = { Amount };
