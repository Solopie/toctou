const { CustomType } = require("./CustomType");

class Balance extends CustomType {
    constructor(value) {
        super();
        this.value = value;
        this.validate();
    }

    isValid() {
        this.value = parseInt(this.value);

        return (
            typeof this.value === "number"
        );
    }
}

module.exports = { Balance };
